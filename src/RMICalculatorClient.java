import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMICalculatorClient
{
    public static void main(String args[])
        throws Exception
    {
        String srvAddr = "localhost";
        int srvPort    = 12345;
        String srvName = args[0];
        String operator    = args[1];
        int number    = Integer.parseInt(args[2]);

        Registry registry = LocateRegistry.getRegistry(srvAddr, srvPort);
        // Registry registry = LocateRegistry.getRegistry();

        RemoteCalculatorInterface rmiServer =
                (RemoteCalculatorInterface) registry.lookup(srvName);

        System.out.println(rmiServer.getClass().getName());

        int reply;
        switch (operator) {
            case "+":
                reply = rmiServer.add(number);
                System.out.println(reply);
                break;
            case "-":
                reply = rmiServer.subtract(number);
                System.out.println(reply);
                break;
            case "*":
                reply = rmiServer.multiply(number);
                System.out.println(reply);
                break;
            case "/":
                reply = rmiServer.divide(number);
                System.out.println(reply);
                break;
            default:
                System.out.println("Hibás operátor");
        }
    }
}
