package producer_buffer_consumer;

import java.util.Random;

public class Producer extends Thread {

    private String name;
    private StringBuffer buffer;
    private static final String[] values = {"a", "josh", "potato", "whatever"};

    public Producer(String name, StringBuffer buffer) {
        this.name = name;
        this.buffer = buffer;
    }

    public void produce(String str) {
        buffer.add(str);
    }

    @Override
    public void run() {
        while (true) {
            Random random = new Random();
            String randomValue = values[random.nextInt(values.length)];
            produce(randomValue);
            System.out.println(name + " produced " + randomValue);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
