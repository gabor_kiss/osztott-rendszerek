package producer_buffer_consumer;

public class Consumer extends Thread {
    private String name;
    private StringBuffer buffer;

    public Consumer(String name, StringBuffer buffer) {
        this.name = name;
        this.buffer = buffer;
    }

    public String consume() {
        return buffer.remove();
    }

    @Override
    public void run(){
        while(true){
            System.out.println(name + " consumed " + consume());
        }
    }
}
