package producer_buffer_consumer;

import java.util.ArrayList;
import java.util.List;

public class StringBuffer {

    private final int max;
    private final List<String> buffer = new ArrayList<String>();

    public StringBuffer(int max) {
        this.max = max;
    }

    public synchronized void add(String value) {
        while (buffer.size() >= max) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ;
        }
        buffer.add(value);
        notify();
    }

    public synchronized String remove() {
        while (buffer.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ;
        }
        String value = buffer.remove(0);
        notify();
        return value;
    }
}
