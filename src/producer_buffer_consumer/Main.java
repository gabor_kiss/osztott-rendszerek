package producer_buffer_consumer;

public class Main {

    public static void main(String[] args) {
        StringBuffer buffer = new StringBuffer(3);

        Thread[] producers = new Producer[4];
        for (int i = 0; i < producers.length; i++) {
            producers[i] = new Producer("prod"+i, buffer);
            producers[i].start();
        }

        Thread consumer = new Consumer("cons1", buffer);
        consumer.start();
    }

}
