package multithreading0402.counter;

public class Counter {
    private int counter = 0;

    public void increment() {
        counter++;
    }

    public int getValue(){
        return counter;
    }
}
