package multithreading0402.counter;

public class Main {

    public static void main(String[] args) {
        Counter counter = new Counter();

        new Thread(new CounterRunner("T1", counter, 1000)).start();
        new Thread(new CounterRunner("T2", counter, 500)).start();
        new Thread(new CounterRunner("T3", counter, 500)).start();
        new Thread(new CounterRunner("T4", counter, 1000)).start();
    }

}
