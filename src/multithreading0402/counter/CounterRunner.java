package multithreading0402.counter;

public class CounterRunner implements Runnable {

    private final String name;
    private final Counter counter;
    private final int iteration;

    public CounterRunner(String name, Counter counter, int iteration) {
        this.name = name;
        this.counter = counter;
        this.iteration = iteration;
    }

    @Override
    public void run() {
        for(int i = 0; i<iteration;++i){
            counter.increment();
        }
        System.out.println(name + " " + counter.getValue());
    }
}
