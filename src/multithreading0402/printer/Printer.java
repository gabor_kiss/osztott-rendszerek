package multithreading0402.printer;

public interface Printer<String> {
    void print(String value);
}
