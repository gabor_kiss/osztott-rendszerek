package multithreading0402.printer;

public class Main {

    public static void main(String[] args){
//        Printer<String> printer = new StandardPrinter();
//        Printer<String> printer = new PrintByCharsPrinter();
        Printer<String> printer = new SynchronizedPrinter();
        new Thread(new DefaultRunner("T1", "Hello", printer, 1000)).start();
        new Thread(new DefaultRunner("T2", "World", printer, 500)).start();
    }

}
