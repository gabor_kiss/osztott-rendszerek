package multithreading0402.printer;

public class DefaultRunner implements Runnable {

    private String name;
    private String value;
    private Printer<String> printer;
    private int iteration;

    public DefaultRunner(String name, String value, Printer<String> printer, int iteration) {
        this.name = name;
        this.value = value;
        this.printer = printer;
        this.iteration = iteration;
    }

    @Override
    public void run(){
        for(int i = 0; i < iteration; ++i){
            printer.print(name + " prints " + value);
        }
    }

}
