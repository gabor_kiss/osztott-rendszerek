package multithreading0402.printer;

public class PrintByCharsPrinter implements Printer<String> {

    @Override
    public void print(String value) {
        for(char c: value.toCharArray()){
            System.out.print(c);
        }
        System.out.println();
    }
}
