package multithreading0402.printer;

public class SynchronizedPrinter implements Printer<String> {

    @Override
    public synchronized void print(String value) {
        for(char c: value.toCharArray()){
            System.out.print(c);
        }
        System.out.println();
    }
}
