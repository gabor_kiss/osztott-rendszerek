
import java.rmi.*;
import java.rmi.registry.*;

public class LottoClient {
    public static void main(String[] args) throws Exception {
        Registry registry = LocateRegistry.getRegistry(12345);

        System.out.println("The available lotto tickets are the following:");

        String[] ticketNames = registry.list();

        System.out.println("The following are the winning tickets:");
        for (String ticketName : ticketNames) {
            LottoTicket lottoTicket = (LottoTicket)registry.lookup(ticketName);

            // Calls remote method.
            boolean isWinning = lottoTicket.isThisTicketWinning();

            if (isWinning)    System.out.println("    - " + ticketName);
        }
    }
}
