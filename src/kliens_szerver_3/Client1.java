package kliens_szerver_3;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client1 {
    public static void main(String[] args) throws UnknownHostException, IOException {
        String HOST = "127.0.0.1";
        int PORT = 12345;
        final String inputPath = "C:\\Users\\Gabor\\IdeaProjects\\osztott-rendszerek\\src\\kliens_szerver_3\\input.txt";
        final String outputPath = "C:\\Users\\Gabor\\IdeaProjects\\osztott-rendszerek\\src\\kliens_szerver_3\\output.txt";

        try (
                Socket s = new Socket(HOST, PORT);
                Scanner sc = new Scanner(s.getInputStream());
                PrintWriter pw = new PrintWriter(s.getOutputStream());
				BufferedReader reader = new BufferedReader(new FileReader(inputPath));
				FileWriter myWriter = new FileWriter(outputPath);
        ) {
			for(String line = reader.readLine(); line != null; line = reader.readLine()){
			    if(line.equals("0")){
                    pw.println(line);
                    pw.flush();
                    break;
                }else{
                    pw.println(line);
                    pw.flush();
                }
                if(sc.hasNextLine()){
                    line = sc.nextLine();
                    System.out.println(line);
                    myWriter.write(line + '\n');
                    myWriter.flush();
                }
			}
        }
    }
}
