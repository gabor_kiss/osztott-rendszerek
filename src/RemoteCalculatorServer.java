import java.rmi.RemoteException;

public class RemoteCalculatorServer
    extends java.rmi.server.UnicastRemoteObject
    implements RemoteCalculatorInterface
{
    static int result;

    public RemoteCalculatorServer() throws RemoteException
    {
        result = 0;
    }

    public int add(int number) throws RemoteException
    {
        return result += number;
    }

    public int subtract(int number) throws RemoteException
    {
        return result -= number;
    }

    public int multiply(int number) throws RemoteException
    {
        return result = result * number;
    }

    public int divide(int number) throws RemoteException
    {
        return result = result / number;
    }
}
