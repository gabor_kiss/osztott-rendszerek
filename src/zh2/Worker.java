package zh2;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Worker {

    public static void main(String[] args) throws IOException {
        try (
                Socket s = new Socket("localhost", 50000);
                Scanner sc = new Scanner(s.getInputStream());
                PrintWriter pw = new PrintWriter(s.getOutputStream());
        ) {
            boolean running = true;
            while (running) {
                if(sc.hasNextLine()){
                    int number = Integer.parseInt(sc.nextLine());
                    System.out.println(number);
                    int prime = largestPrime(number);
                    System.out.println(prime);
                    pw.println(prime);
                    pw.flush();
                }
                if(sc.hasNextLine()){
                    System.out.println(sc.nextLine());
                    running = false;
                }
            }
        }
    }

    static int largestPrime(int k){
        for(int i=k; i>=2; --i){
            if(isPrime(i)){ return i; }
        }
        return 2;
    }

    static boolean isPrime(int k){
        for(int i=2; i<k; ++i){
            if(k % i == 0) { return false; }
        }
        return true;
    }
}
