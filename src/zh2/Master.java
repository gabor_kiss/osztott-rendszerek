package zh2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Master {

    public static void main(String[] args) throws IOException {
        LinkedList<Integer> numbers = new LinkedList<>();
        LinkedList<Client> clients = new LinkedList<>();
        ArrayList<Integer> primes = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("zh2\\" + args[0]))) {
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                System.out.println(line);
                numbers.add(Integer.parseInt(line));
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file does not exist!");
        }

        try (
                ServerSocket serverSocket = new ServerSocket(50000)
        ) {
            int clientNumber = numbers.size();
            while (clientNumber > 0) {
                try {
                    Socket client = serverSocket.accept();
                    PrintWriter pw = new PrintWriter(client.getOutputStream());
                    Scanner sc = new Scanner(client.getInputStream());

                    clients.add(new Client(client, pw, sc));
                    pw.println(numbers.poll());
                    pw.flush();
                    clientNumber--;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            boolean running = true;
            while (running) {
                Client client = clients.poll();
                if (client != null) {
                    if (client.sc.hasNextLine()) {
                        primes.add(Integer.parseInt(client.sc.nextLine()));
                        client.pw.println("OK");
                        client.pw.flush();
                        closeConnection(client);
                    }
                } else {
                    running = false;
                }
            }
            System.out.println(primes);
        }
    }

    private static void closeConnection(Client client) throws IOException {
        client.sc.close();
        client.pw.close();
        client.clientSocket.close();
    }

}

class Client {
    Socket clientSocket;
    PrintWriter pw;
    Scanner sc;

    public Client(Socket clientSocket, PrintWriter pw, Scanner sc) {
        this.clientSocket = clientSocket;
        this.pw = pw;
        this.sc = sc;
    }
}
