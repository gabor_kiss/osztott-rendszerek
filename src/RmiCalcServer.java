
import java.rmi.*;
import java.rmi.server.*;

public class RmiCalcServer extends UnicastRemoteObject implements RmiCalcInterface
{
    private int number;

    public RmiCalcServer() throws RemoteException
    {
        this.number = 0;
    }

    public RmiCalcServer(int number) throws RemoteException
    {
        this.number = number;
    }

    public RmiCalcServer(RmiCalcServer other) throws RemoteException
    {
        this.number = other.number;
    }

    public int getNumber() throws RemoteException {
        return number;
    }

    public void setNumber(int number) throws RemoteException {
        this.number = number;
    }

    public void add(int number) throws RemoteException {
        this.number += number;
    }

    public void subtract(int number) throws RemoteException {
        this.number -= number;
    }

}
