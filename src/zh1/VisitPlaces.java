package zh1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class VisitPlaces {
    private static final HashSet<SightseeingObject> objects = new HashSet<>();

    public static void main(String[] args) throws IOException {
        processFile(args[0]);
        try (
                ServerSocket serverSocket = new ServerSocket(8324);
        ) {
            boolean running = true;
            while (running) {
                try (
                        Socket client = serverSocket.accept();
                        Scanner sc = new Scanner(client.getInputStream());
                        PrintWriter pw = new PrintWriter(client.getOutputStream());
                ) {
                    boolean connected = true;
                    while (connected) {
                        if (sc.hasNextLine()) {
                            String option = sc.nextLine();
                            String[] splitOption = option.split(" ");
                            switch (splitOption[0]) {
                                case "done":
                                    client.close();
                                    sc.close();
                                    pw.close();
                                    connected = false;
                                    break;
                                case "exit":
                                    running = false;
                                    connected = false;
                                    break;
                                case "visit":
                                    refreshObjects(visitMonument(splitOption[1]));
                                    for (SightseeingObject object : objects) {
                                        if (object.monument.name.equals(splitOption[1])) {
                                            pw.println(object.visits);
                                            pw.flush();
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            printVisits();
        }
    }

    private static void printVisits() {
        HashMap<String, Integer> monuments = new HashMap<>();
        HashMap<String, Integer> cities = new HashMap<>();
        HashMap<String, Integer> monumentTypes = new HashMap<>();

        for (SightseeingObject object : objects) {
            monuments.put(object.monument.name, object.monument.visits);
            String cityName = object.city.name;
            if (cities.get(cityName) == null) {
                cities.put(cityName, object.city.visits);
            }
            String monumentTypeName = object.monumentType.name;
            if (monumentTypes.get(monumentTypeName) == null) {
                monumentTypes.put(monumentTypeName, object.monumentType.visits);
            }
        }

        System.out.println(monuments);
        System.out.println(cities);
        System.out.println(monumentTypes);
    }

    private static void processFile(String fileName) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("zh1\\resources\\" + fileName))) {
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                String[] splittedLine = line.split(",");
                objects.add(
                        new SightseeingObject(
                                new Monument(splittedLine[0]),
                                new City(splittedLine[1]),
                                new MonumentType(splittedLine[2]))
                );
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file does not exist!");
        }
    }

    private static SightseeingObject visitMonument(String monument) {
        SightseeingObject returnObject = null;
        for (SightseeingObject object : objects) {
            if (object.monument.name.equals(monument)) {
                returnObject = object;
                object.monument.visits = object.monument.visits + 1;
                object.city.visits = object.city.visits + 1;
                object.monumentType.visits = object.monumentType.visits + 1;
                object.visits = object.visits + 1;
            }
        }
        return returnObject;
    }

    private static void refreshObjects(SightseeingObject sightseeingObject) {
        for (SightseeingObject object : objects) {
            if (!object.monument.name.equals(sightseeingObject.monument.name)
                    && object.city.name.equals(sightseeingObject.city.name)) {
                object.city.visits = object.city.visits + 1;
            }
            if (!object.monument.name.equals(sightseeingObject.monument.name)
                    && object.monumentType.name.equals(sightseeingObject.monumentType.name)) {
                object.monumentType.visits = object.monumentType.visits + 1;
            }
        }
    }

    public static class SightseeingObject {
        Monument monument;
        City city;
        MonumentType monumentType;
        int visits = 0;

        public SightseeingObject(Monument monument, City city, MonumentType monumentType) {
            this.monument = monument;
            this.city = city;
            this.monumentType = monumentType;
        }

        @Override
        public String toString() {
            return monument.toString() + ", " + city.toString() + ", " + monumentType.toString() + "\n";
        }
    }

    public static class Monument {
        String name;
        int visits = 0;

        public Monument(String name) {
            this.name = name;
        }
    }

    public static class City {
        String name;
        int visits = 0;

        public City(String name) {
            this.name = name;
        }
    }

    public static class MonumentType {
        String name;
        int visits = 0;

        public MonumentType(String name) {
            this.name = name;
        }
    }
}
