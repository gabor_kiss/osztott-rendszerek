package zh1.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        try (
                Socket s = new Socket("localhost", 8324);
                Scanner sc = new Scanner(s.getInputStream());
                PrintWriter pw = new PrintWriter(s.getOutputStream());
        ) {
            while (true) {
                String toSend = scanner.nextLine();
                pw.println(toSend);
                pw.flush();
                if (toSend.equals("done")) {
                    System.out.println("A szerver bontotta a kapcsolatot");
                    break;
                }
                if (toSend.equals("exit")) {
                    System.out.println("A kliens lezárta a szervert");
                    break;
                }
                if (sc.hasNextLine()) {
                    System.out.println(sc.nextLine());
                }
            }
        }
    }
}