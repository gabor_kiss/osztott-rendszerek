import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMICalculatorDeploy
{
    public static void main(String args[])
        throws Exception
    {
        final int PORT = 12345;

        Registry registry = LocateRegistry.createRegistry(PORT);
        // Registry registry = LocateRegistry.getRegistry();

        registry.rebind("rmiAddCalculator", new RemoteCalculatorServer());
     }
}
