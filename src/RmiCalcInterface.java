
import java.rmi.*;

// az interfészek/osztályok neve ne maradjon "AppendTxt"
public interface RmiCalcInterface extends Remote
{
      // v1: ez egy hackelős megoldás lenne, nem javasolt
//    String doAnyComputationILike(String allArgumentsMashedTogether) throws RemoteException;

    // v2: ez túl részletekbe menő lenne
    // void pressDigit1() throws RemoteException;
    // void pressDigit2() throws RemoteException;
    // void pressDigit3() throws RemoteException;
    // void pressDigit...() throws RemoteException;

    // v3: ez a típushiba: nem három számot adunk össze
    // int add(int num1, int num2) throws RemoteException;

    // v4: így jó
    int getNumber() throws RemoteException;
    void setNumber(int number) throws RemoteException;
    void add(int number) throws RemoteException;
    void subtract(int number) throws RemoteException;
    // ... és további műveletek


    // v5: úgy is jó, ha mindig megkapjuk az aktuális értéket
    // void setNumber(int number) throws RemoteException;
    // int add(int number) throws RemoteException;
    // int subtract(int number) throws RemoteException;

    // v6: úgy is jó, ha kevés üzenetet akarunk átküldeni, és több műveletet hajtunk végre egyszerre
    // void setNumber(int number) throws RemoteException;
    // int doOperations(List<Operation> operations) throws RemoteException;

    // v6-hoz (egy külön fájlba) kell egy ilyen osztály is:
    // public class Operation implements Serializable {
    //      ...
    // }
    // ... és leszármazottai minden művelethez:
    // public class AddOperation extends Operation {
    //      ...
    // }
}
