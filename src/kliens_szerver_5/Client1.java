package kliens_szerver_5;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client1 {
    public static void main(String[] args) throws UnknownHostException, IOException {
        String HOST = "127.0.0.1";
        int PORT = 12345;

        try (
                Socket s = new Socket(HOST, PORT);
                Scanner sc = new Scanner(s.getInputStream());
                PrintWriter pw = new PrintWriter(s.getOutputStream());
        ) {
			if(sc.hasNextLine()){
                System.out.println(sc.nextLine());
            }
        }
    }
}
