package kliens_szerver_5;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server1 {
    private static int clientCount = 0;
    public static void main(String[] args) throws IOException {
        final int PORT = 12345;

        System.out.println("Server started at port: " + PORT);
        try (
                ServerSocket ss = new ServerSocket(PORT);
        ) {
            while (true) {
                try (
                        Socket s = ss.accept();
                        Scanner sc = new Scanner(s.getInputStream());
                        PrintWriter pw = new PrintWriter(s.getOutputStream());
                ) {
                    clientCount++;
                    pw.println(clientCount);
                    pw.flush();
                }
            }
        }
    }
}
