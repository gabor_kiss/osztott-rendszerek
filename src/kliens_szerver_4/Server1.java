package kliens_szerver_4;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server1 {
	public static void main(String[] args) throws IOException {
		final int PORT = 12345;

		ServerSocket ss = new ServerSocket(PORT);

        System.out.println("Server started at port: " + PORT);
		try (
				Socket s = ss.accept();
				Scanner sc = new Scanner(s.getInputStream());
				PrintWriter pw = new PrintWriter(s.getOutputStream());
		) {
			if(sc.hasNextLine()) {
				String inputName = sc.nextLine();
				try(BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\uurjze\\Desktop\\oszrend\\src\\kliens_szerver_4\\" + inputName))){
					for(String line = reader.readLine(); line != null; line = reader.readLine()){
						pw.println(line);
						pw.flush();
					}
				}catch (FileNotFoundException e){
					pw.println("The file does not exist!");
					pw.flush();
				}
			}
		}
	}
}
