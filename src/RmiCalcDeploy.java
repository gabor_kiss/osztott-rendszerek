
import java.rmi.registry.*;

public class RmiCalcDeploy
{
    public static void main(String args[])
        throws Exception
    {
        final int PORT = 12345;

        Registry registry = LocateRegistry.createRegistry(PORT);

        RmiCalcServer calc1 = new RmiCalcServer();

        registry.bind("calc1", calc1);
        registry.bind("calc2", new RmiCalcServer(123));
        registry.bind("calc3", new RmiCalcServer(calc1));
     }
}
