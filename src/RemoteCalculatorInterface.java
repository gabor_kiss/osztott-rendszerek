// Remote Method Invocation
// Távoli metódushívás

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteCalculatorInterface extends Remote
{
    int add(int number) throws RemoteException;
    int subtract(int number) throws RemoteException;
    int multiply(int number) throws RemoteException;
    int divide(int number) throws RemoteException;
    // MyData appendTxt(MyData str) throws RemoteException;
}

// class MyData implements Serializable
