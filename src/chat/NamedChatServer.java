package chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class NamedChatServer {

    private static List<Client> connections = new ArrayList<>();
    private static BlockingQueue<Message> messageQueue = new LinkedBlockingQueue<>();
    private ServerSocket serverSocket;

    public static void main(String[] args) throws Exception {
        NamedChatServer namedChatServer = new NamedChatServer(new ServerSocket(5056));
        namedChatServer.createConnection();
        namedChatServer.sendMessages();
    }

    private NamedChatServer(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    private void createConnection() {
        new Thread(new Runnable() {
            @Override public void run() {
                while (true) {
                    try {
                        Client newClient = new Client(serverSocket.accept());
                        connections.add(newClient);
                        System.out.println("A new client is connected : " + newClient.name);
                        System.out.println("Assigning new thread for this client");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void sendMessages() {
        new Thread(new Runnable() {
            @Override public void run() {
                while (true) {
                    try {
                        Message message = messageQueue.take();
                        connections.stream()
                            .filter(client -> !client.getName().equals(message.name))
                            .forEach(client -> {
                                try {
                                    client.sendMessage(message.name + ": " + message.message);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }

    static class Client {
        private Socket clientSocket;
        private DataOutputStream dos;
        private DataInputStream dis;
        private String name;

        Client(Socket socket) throws IOException {
            clientSocket = socket;
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
            askName();
            readThread();
        }

        void askName() throws IOException {
            name = dis.readUTF();
        }

        String getName() {
            return name;
        }

        private void readThread() {
            new Thread(new Runnable() {
                @Override public void run() {
                    while (!clientSocket.isClosed()) {
                        try {
                            String message = dis.readUTF();
                            if ("exit".equals(message)) {
                                Client.this.closeClient();
                                messageQueue.add(new Message("disconnected", name));
                                System.out.println(name + ": disconnected");
                            } else {
                                messageQueue.add(new Message(message, name));
                                System.out.println(name + ": " + message);
                            }
                        } catch (IOException ex) {
                            Client.this.closeClient();
                        }
                    }
                }
            }).start();
        }

        private void closeClient() {
            try {
                dis.close();
                dos.close();
                clientSocket.close();
                connections.remove(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void sendMessage(String message) throws IOException {
            dos.writeUTF(message);
            dos.flush();
        }

    }

    static class Message {
        private String message;
        private String name;

        Message(String message, String fromName) {
            this.message = message;
            this.name = fromName;
        }

    }

}
