package chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class MultiServerChatClient {

    private static Socket socket;
    private static DataInputStream dis;
    private static DataOutputStream dos;
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            int PORT = Integer.parseInt(args[0]);
            socket = new Socket(InetAddress.getByName("localhost"), PORT);
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());
            readThread();
            writeThread();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readThread() {
        new Thread(new Runnable() {
            @Override public void run() {
                while (!socket.isClosed()) {
                    try {
                        System.out.println(dis.readUTF());
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        }).start();
    }

    private static void writeThread() {
        new Thread(new Runnable() {
            @Override public synchronized void run() {
                while (!socket.isClosed()) {
                    try {
                        System.out.print("> ");
                        String message = scanner.nextLine();
                        dos.writeUTF(message);
                        dos.flush();
                        if ("exit".equals(message)) {
                            closeResources();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private static void closeResources() throws IOException {
        dos.close();
        dis.close();
        socket.close();
    }
}