package chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class AsyncChatClient {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 5056);
            readThread(new DataInputStream(socket.getInputStream())).start();
            writeThread(new DataOutputStream(socket.getOutputStream())).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Thread readThread(DataInputStream dis) {
        return new Thread(new Runnable() {
            @Override public void run() {
                while (true) {
                    try {
                        String message = dis.readUTF();
                        System.out.println(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private static Thread writeThread(DataOutputStream dos) {
        return new Thread(new Runnable() {
            @Override public void run() {
                while (true) {
                    try {
                        String toSend = scanner.nextLine();
                        dos.writeUTF(toSend);
                        dos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}
