package chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MultiServerChatServer1 {

    private static List<Client> connections = new ArrayList<>();
    private static BlockingQueue<Message> messageQueueOut = new LinkedBlockingQueue<>();
    private static BlockingQueue<Message> messageQueueIn = new LinkedBlockingQueue<>();
    private ServerSocket serverSocket;
    private static DataInputStream dis;
    private static DataOutputStream dos;
    private Socket socket;

    public static void main(String[] args) throws Exception {
        String address = args[0];
        int PORT = Integer.parseInt(args[1]);
        MultiServerChatServer1 multiServerChatServer1 = new MultiServerChatServer1(new ServerSocket(5057),new Socket(address, PORT));
        multiServerChatServer1.createServerConnection();
        multiServerChatServer1.createConnection();
        multiServerChatServer1.sendToServer();
        multiServerChatServer1.acceptFromServer();
        multiServerChatServer1.sendMessages();
    }

    private MultiServerChatServer1(ServerSocket serverSocket, Socket socket) {
        this.serverSocket = serverSocket;
        this.socket = socket;
    }

    private void createServerConnection() throws IOException {
        this.dis = new DataInputStream(socket.getInputStream());
        this.dos = new DataOutputStream(socket.getOutputStream());
        try {
            Message message = new Message("The server has connected", "server");
            dos.writeUTF(message.name + ": " + message.message);
            dos.flush();
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    private void sendToServer() {
        new Thread(new Runnable() {
            @Override public void run() {
                while (true) {
                    try {
                        Message message = messageQueueOut.take();
                        try {
                            dos.writeUTF(message.name + ": " + message.message);
                            dos.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void acceptFromServer() {
        new Thread(new Runnable() {
            @Override public void run() {
                while (true) {
                    try {
                        String message = dis.readUTF();
                        String[] parts = message.split(": ");
                        Message m = new Message(parts[1], parts[0]);
                        messageQueueIn.add(m);
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void createConnection() {
        new Thread(new Runnable() {
            @Override public void run() {
                while (true) {
                    try {
                        Client newClient = new Client(serverSocket.accept());
                        connections.add(newClient);
                        System.out.println("A new client is connected : " + newClient.name);
                        System.out.println("Assigning new thread for this client");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void sendMessages() {
        new Thread(new Runnable() {
            @Override public void run() {
                while (true) {
                    try {
                        Message message = messageQueueIn.take();
                        connections.stream()
                            .filter(client -> !client.getName().equals(message.name))
                            .forEach(client -> {
                                try {
                                    client.sendMessage(message.name + ": " + message.message);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }

    static class Client {
        private Socket clientSocket;
        private DataOutputStream dos;
        private DataInputStream dis;
        private String name;

        Client(Socket socket) throws IOException {
            clientSocket = socket;
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
            askName();
            readThread();
        }

        void askName() throws IOException {
            name = dis.readUTF();
        }

        String getName() {
            return name;
        }

        private void readThread() {
            new Thread(new Runnable() {
                @Override public void run() {
                    while (!clientSocket.isClosed()) {
                        try {
                            String message = dis.readUTF();
                            if ("exit".equals(message)) {
                                Client.this.closeClient();
                                messageQueueOut.add(new Message("disconnected", name));
                                System.out.println(name + ": disconnected");
                            } else {
                                messageQueueOut.add(new Message(message, name));
                                System.out.println(name + ": " + message);
                            }
                        } catch (IOException ex) {
                            Client.this.closeClient();
                        }
                    }
                }
            }).start();
        }

        private void closeClient() {
            try {
                dis.close();
                dos.close();
                clientSocket.close();
                connections.remove(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void sendMessage(String message) throws IOException {
            dos.writeUTF(message);
            dos.flush();
        }

    }

    static class Message {
        private String message;
        private String name;

        Message(String message, String fromName) {
            this.message = message;
            this.name = fromName;
        }

    }

}