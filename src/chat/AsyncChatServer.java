package chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class AsyncChatServer {

    public static void main(String[] args) throws Exception {
        try (ServerSocket serverSocket = new ServerSocket(5056)) {
            Socket socket1 = serverSocket.accept();
            Socket socket2 = serverSocket.accept();
            connectClients(socket1, socket2);
        }
    }

    private static void connectClients(Socket socket1, Socket socket2) throws IOException {
        readThread(new DataInputStream(socket1.getInputStream()), new DataOutputStream(socket2.getOutputStream())).start();
        readThread(new DataInputStream(socket2.getInputStream()), new DataOutputStream(socket1.getOutputStream())).start();
    }

    private static Thread readThread(DataInputStream dis, DataOutputStream dos) {
        return new Thread(new Runnable() {
            @Override public void run() {
                while (true) {
                    try {
                        String message = dis.readUTF();
                        dos.writeUTF(message);
                        dos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}
