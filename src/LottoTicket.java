
import java.rmi.*;

public interface LottoTicket extends Remote {
    public boolean isThisTicketWinning() throws RemoteException;
}
