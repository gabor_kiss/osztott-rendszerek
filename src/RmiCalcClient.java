
import java.rmi.*;
import java.rmi.registry.*;

public class RmiCalcClient
{
    public static void main(String args[])
        throws Exception
    {
        String srvAddr = "localhost";
        int srvPort    = 12345;

        Registry registry = LocateRegistry.getRegistry(srvAddr, srvPort);

        RmiCalcInterface remoteCalc1 = (RmiCalcInterface)(registry.lookup("calc1"));
        RmiCalcInterface remoteCalc2 = (RmiCalcInterface)(registry.lookup("calc2"));
        RmiCalcInterface remoteCalc3 = (RmiCalcInterface)(registry.lookup("calc3"));

        System.out.printf("%d %d %d%n", remoteCalc1.getNumber(), remoteCalc2.getNumber(), remoteCalc3.getNumber());
        remoteCalc1.setNumber(12345);
        System.out.printf("%d %d %d%n", remoteCalc1.getNumber(), remoteCalc2.getNumber(), remoteCalc3.getNumber());
        remoteCalc2.add(567);
        System.out.printf("%d %d %d%n", remoteCalc1.getNumber(), remoteCalc2.getNumber(), remoteCalc3.getNumber());
        remoteCalc3.subtract(999);
        System.out.printf("%d %d %d%n", remoteCalc1.getNumber(), remoteCalc2.getNumber(), remoteCalc3.getNumber());
    }
}
