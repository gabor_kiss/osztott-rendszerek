package shop;

import java.rmi.*;
import java.rmi.registry.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class UnitDeploy {
    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            createUnits();
        } else {
            setUnit(args);
        }
    }

    private static void createUnits() throws Exception {
        Registry reg = LocateRegistry.createRegistry(33333);

        for (int i = 1; i <= 5; ++i) {
            int cost = ThreadLocalRandom.current().nextInt(100, 500);
            reg.bind("unit" + i, new UnitImpl(cost));
        }
    }

    private static void setUnit(String[] args) throws Exception {
        Registry reg = LocateRegistry.getRegistry(33333);

        for (int i = 0; i < args.length / 2; i += 2) {
            String unitname = args[i];
            int cost = Integer.parseInt(args[i+1]);

            reg.rebind(unitname, new UnitImpl(cost));

            System.out.printf("Setting %s = %d%n", unitname, cost);
        }
    }
}
