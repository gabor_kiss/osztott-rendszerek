package shop;

import java.rmi.*;

public interface Unit extends Remote {
    public int getCost() throws RemoteException;
}
