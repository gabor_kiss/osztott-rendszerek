package shop;

import java.io.*;
import java.util.*;
import java.net.*;
import java.sql.*;
import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;

public class ShopDemo {
    static class DbConfig {
        public String url;
        public String user;
        public String password;

        public DbConfig(String url, String user, String password) {
            this.url = url;
            this.user = user;
            this.password = password;
        }
    }


    public static DbConfig db = new DbConfig("jdbc:hsqldb:file:hsqldb_webshop/webshop", "sa", "");
    public static Registry registry;

    public static boolean isRunning = true;

    public static void main(String[] args) throws Exception {
        registry = LocateRegistry.getRegistry(33333);
        boolean keepData = arrayContains(args, "keep");
        boolean useTimeout = arrayContains(args, "timeout");
        int timeoutMillis = 1000;

        int PORT = 12345;
        try (
                ServerSocket ss = new ServerSocket(12345);
                Connection conn = DriverManager.getConnection(db.url, db.user, db.password);
        ) {
            if (keepData) {
                System.out.println("Keeping database...");
            } else {
                System.out.println("Clearing database...");
                initTables(conn);
            }

            if (useTimeout) {
                System.out.printf("Will use a timeout of %d millis%n", timeoutMillis);
                ss.setSoTimeout(timeoutMillis);
            }

            while (isRunning) {
                try {
                    Socket s = ss.accept();

                    if (!isRunning) break;

                    new Thread(() -> {
                        try (
                                s;
                                Scanner sc = new Scanner(s.getInputStream());
                                PrintWriter pw = new PrintWriter(s.getOutputStream());
                                Statement stmt = conn.createStatement();
                        ) {
                            handleClient(s, sc, pw, conn);
                        } catch (Exception e) {
                            // "logging"
                            e.printStackTrace();
                        }
                    }).start();
                } catch (SocketTimeoutException e) {
                    // this is expected, nothing to do
                }
            }
        }
    }

    private static void handleClient(Socket s, Scanner sc, PrintWriter pw, Connection conn) throws Exception {
        pw.println("Enter username:");
        pw.flush();

        String username = sc.nextLine();

        while (isRunning && sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] parts = line.split(" ");

            if (parts[0].equals("buy")) {
                String productname = parts[1];
                int amount = Integer.parseInt(parts[2]);

                int unitcost = getUnitCost(productname);

                try (
                        PreparedStatement prep = conn.prepareStatement("insert into unit values (?, ?, ?, ?);");
                ) {
                    prep.setString(1, username);
                    prep.setString(2, productname);
                    prep.setInt(3, amount);
                    prep.setInt(4, unitcost);
                    prep.addBatch();
                    prep.executeBatch();

                    pw.printf("Bought %d units for %d%n", amount, unitcost);
                }
            } else if (parts[0].equals("sell")) {
                String productname = parts[1];
                int amount = Integer.parseInt(parts[2]);

                int unitcost = getUnitCost(productname);

                try (
                        PreparedStatement prep = conn.prepareStatement("insert into unit values (?, ?, ?, ?);");
                ) {
                    prep.setString(1, username);
                    prep.setString(2, productname);
                    prep.setInt(3, -amount);
                    prep.setInt(4, unitcost);
                    prep.addBatch();
                    prep.executeBatch();

                    pw.printf("Bought %d units for %d%n", amount, unitcost);
                }
            } else if (parts[0].equals("sum")) {
                try (
                        Statement stmt = conn.createStatement();
                        ResultSet rs = stmt.executeQuery("select * from unit");
                ) {
                    int sum = 0;
                    int unitCount = 0;
                    while (rs.next()) {
                        int amount = rs.getInt("amount");
                        int unitcost = rs.getInt("unitcost");
                        sum += amount * unitcost;
                        unitCount += amount;
                    }

                    pw.printf("Total cost = %d, units = %d%n", sum, unitCount);
                }
            } else if (parts[0].equals("buyers")) {
                try (
                        Statement stmt = conn.createStatement();
                        ResultSet rs = stmt.executeQuery("select * from unit");
                ) {
                    List<String> buyers = new ArrayList<>();

                    while (rs.next()) {
                        String buyer = rs.getString("buyername");
                        buyers.add(buyer);
                    }

                    pw.println("Buyers: " + String.join(", ", buyers));
                }
            } else if (parts[0].equals("exit")) {
                break;
            } else if (parts[0].equals("quit")) {
                isRunning = false;
                break;
            } else {
                pw.println("Unknown command: " + parts[0]);
            }

            pw.flush();
        }
    }

    private static void initTables(Connection conn) throws SQLException {
        try (
                Statement stmt = conn.createStatement();
        ) {
            stmt.executeUpdate("drop table if exists unit;");
            stmt.executeUpdate("create table unit (buyername varchar(80), productname varchar(80), amount int, unitcost int);");
        }
    }

    private static int getUnitCost(String productname) throws Exception {
        try {
            Unit unit = (Unit) registry.lookup(productname);
            return unit.getCost();
        } catch (Exception e) {
            // this illustrates what no to do in production code
            return 0;
        }
    }

    private static boolean arrayContains(String[] array, String value) {
        for (String elem : array) {
            if (elem.equals(value)) return true;
        }
        return false;
    }

}
