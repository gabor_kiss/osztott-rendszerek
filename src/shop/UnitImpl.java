package shop;

import java.rmi.*;
import java.rmi.server.*;

public class UnitImpl extends UnicastRemoteObject implements Unit {
    int cost;

    public UnitImpl(int cost) throws RemoteException
    {
        this.cost = cost;
    }

    public int getCost() throws RemoteException {
        return cost;
    }
}
