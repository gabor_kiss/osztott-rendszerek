
import java.rmi.*;
import java.rmi.server.*;

public class LottoTicketImpl extends UnicastRemoteObject implements LottoTicket {
    boolean isThisTicketWinning;

    public LottoTicketImpl(boolean isThisTicketWinning) throws RemoteException
    {
    	this.isThisTicketWinning = isThisTicketWinning;
    }

    public boolean isThisTicketWinning() throws RemoteException {
        return isThisTicketWinning;
    }
}
