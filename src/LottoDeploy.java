
import java.rmi.*;
import java.rmi.registry.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class LottoDeploy {
    public static void main(String[] args) throws Exception {
        int ticketCount = 90;
        int winnerCount = 5;

        LocateRegistry.createRegistry(12345);

        // use either of the two methods to create winners
        Collection<Integer> winningTickets = ThreadLocalRandom.current().nextBoolean() ?
            makeWinningTickets(ticketCount, winnerCount) :
            makeWinningTicketsV2(ticketCount, winnerCount);

        deployTickets(ticketCount, winningTickets);

        System.out.println("Use RMI to find the winning lottery tickets");
    }

    private static Collection<Integer> makeWinningTickets(int ticketCount, int winnerCount) {
        Set<Integer> winningTickets = new HashSet<Integer>();
        while (winningTickets.size() < winnerCount)
        {
            winningTickets.add(ThreadLocalRandom.current().nextInt(ticketCount) + 1);
        }
        return winningTickets;
    }

    private static Collection<Integer> makeWinningTicketsV2(int ticketCount, int winnerCount) {
        List<Integer> allTickets = new ArrayList<>();
        for (int i = 1; i <= ticketCount; ++i) {
            allTickets.add(i);
        }

        Collections.shuffle(allTickets);

        return allTickets.subList(0, winnerCount);
    }

    private static void deployTickets(int ticketCount, Collection<Integer> winningTickets) throws Exception {
        Registry registry = LocateRegistry.getRegistry(12345);

        System.out.println("Note: the winning tickets are the following: " + winningTickets);

        for (int i = 1; i <= ticketCount; ++i) {
            boolean isThisTicketWinning = winningTickets.contains(i);
            LottoTicket ticket = new LottoTicketImpl(isThisTicketWinning);
            registry.rebind("Lotto" + i, ticket);
        }
    }
}
