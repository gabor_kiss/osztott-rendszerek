package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// you may use this if your Java is older than v14
class DbConfig {
    public String url;
    public String user;
    public String password;

    public DbConfig(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }
}

// needs sqlite-jdbc-VERSIONNUMBER.jar or hsqldb.jar on the classpath
// run as: java --enable-preview --source 14 -cp .;sqlite-jdbc-VERSIONNUMBER.jar DbExample.java
//     or: java --enable-preview --source 14 -cp .;hsqldb.jar DbExample.java
public class DbExample {
    public static void main(String[] args) throws Exception {
        // Java 14 preview feature (if left uncommented, hides the DbConfig class)
//        record DbConfig(String user, String password, String url) {}

//    	DbConfig db = new DbConfig("abc", "", "jdbc:sqlite:sqlite_test.db");
        DbConfig db = new DbConfig("jdbc:hsqldb:file:hsqldb_dir/hsql_test_db", "SA", "");

        try (
                Connection conn = DriverManager.getConnection(db.url, db.user, db.password);
        ) {
            createTables(conn);
            insertData(conn);
            queryData(conn);
            queryData2(conn);
            queryData3(conn);

            // these queries can be used as intended...
            dangerousQuerySqlInjection(conn, "1890");
            queryData(conn);
            dangerousQuerySqlInjection2(conn, "1900");
            queryData(conn);

            // ... but they can be easily misused (hint: sanitize data e.g. with PreparedStatement)
            dangerousQuerySqlInjection(conn, "'; update people set name = 'HACKED'; --");
            queryData(conn);
            dangerousQuerySqlInjection2(conn, "'; update people set name = 'HACKED AGAIN'; --");
            queryData(conn);
        }
    }

    private static void createTables(Connection conn) throws SQLException {
        try (
                Statement stat = conn.createStatement();
        ) {
            stat.executeUpdate("drop table if exists people;");
            stat.executeUpdate("create table people (name varchar(80), birthyear int);");
        }
    }

    private static void insertData(Connection conn) throws SQLException {
        try (
                PreparedStatement prep = conn.prepareStatement("insert into people values (?, ?);");
        ) {
            addPerson(prep, "Gandhi", 1869);
            addPerson(prep, "Turing", 1912);
            addPerson(prep, "Wittgenstein", 1889);
            addPerson(prep, "Frege", 1848);

            conn.setAutoCommit(false);
            prep.executeBatch();
            conn.setAutoCommit(true);
        }
    }

    private static void addPerson(PreparedStatement prep, String name, int birthyear) throws SQLException {
        prep.setString(1, name);
        prep.setInt(2, birthyear);
        prep.addBatch();
    }

    private static void queryData(Connection conn) throws SQLException {
        try (
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery("select * from people;");
        ) {
            while (rs.next()) {
                String name = rs.getString("name");
                int birthyear = rs.getInt("birthyear");
                System.out.printf("name = %s, birthyear = %d%n", name, birthyear);
            }
        }
    }

    private static void queryData2(Connection conn) throws SQLException {
        try (
                PreparedStatement p = conn.prepareStatement("select * from people p where p.birthyear < ? and p.name <> ?;");
        ) {
            p.setInt(1, 1900);
            p.setString(2, "Wittgenstein");

            try (
                    ResultSet rs = p.executeQuery();
            ) {
                while (rs.next()) {
                    String name = rs.getString("name");
                    int birthyear = rs.getInt("birthyear");
                    System.out.printf("name = %s, birthyear = %d%n", name, birthyear);
                }
            }
        }
    }

    private static void queryData3(Connection conn) throws SQLException {
        try (
                PreparedStatement p = conn.prepareStatement("select count(*) from people p where p.birthyear < ? and p.name <> ?;");
        ) {
            p.setInt(1, 1900);
            p.setString(2, "Wittgenstein");

            try (
                    ResultSet rs = p.executeQuery();
            ) {
                rs.next();
                int resultCount = rs.getInt(1);
                System.out.println("count = " + resultCount);
            }
        }
    }

    private static void dangerousQuerySqlInjection(Connection conn, String unsafeParam) throws SQLException {
        try (
                Statement stmt = conn.createStatement();
        ) {
            stmt.executeUpdate("update people set name = name || ' (born before " + unsafeParam + ")' where birthyear < " + unsafeParam + ";");
        }
    }

    // wrong even like this
    private static void dangerousQuerySqlInjection2(Connection conn, String unsafeParam) throws SQLException {
        try (
                Statement stmt = conn.createStatement();
        ) {
            String fmt = String.format("update people set name = name || ' (born after %1$s)' where birthyear < %1$s;", unsafeParam);
            stmt.executeUpdate(fmt);
        }
    }

}

